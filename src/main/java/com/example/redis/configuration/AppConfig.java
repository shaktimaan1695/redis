package com.example.redis.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class AppConfig {

  @Bean
  public JedisConnectionFactory redisConnectionFactory() {
    JedisConnectionFactory jedis = new JedisConnectionFactory();
    jedis.setHostName("localhost");
    jedis.setPort(6379);
    return jedis;
  }

  @Bean(name = "redisTemplate")
  public RedisTemplate<String, Object> getRedisTemplate() {
    RedisTemplate<String, Object> jedis = new RedisTemplate<String, Object>();
    JedisConnectionFactory jedisConnectionFactory = redisConnectionFactory();
    jedis.setConnectionFactory(jedisConnectionFactory);
    return jedis;
  }


}
