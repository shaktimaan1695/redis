package com.example.redis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
public class RedisService {

@Autowired RedisTemplate redisTemplate;


@RequestMapping("/save")
public void save() {
  redisTemplate.opsForHash().put("items", "123", "BlackTees");
}
}
